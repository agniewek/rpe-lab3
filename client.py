import books_pb2
import zmq
import time


context = zmq.Context()

#  Socket to talk to server
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:5555")

# Creating message

book = books_pb2.Book()
book.title = "The Title"
book.pages = 100
book.price = 1.1

msg = book.SerializeToString()
time.sleep(1)
for i in range(10):
    print("Sending book:\n%s" % book)
    socket.send(msg)
