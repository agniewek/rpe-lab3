import books_pb2
import zmq


context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://localhost:5555")
socket.setsockopt(zmq.SUBSCRIBE, '')
while True:
    #  Wait for next request from client
    print("waiting")
    message = socket.recv()
    book = books_pb2.Book()
    book.ParseFromString(message)
    print("Received book:\n%s" % book)
